const c = document.getElementById("canvas");
const ctx = c.getContext("2d");

// Inputs
const slider_system_num = document.getElementById("slider_system_num");
const input_system_num = document.getElementById("input_system_num");

const slider_system_entropy = document.getElementById("slider_system_entropy");
const input_system_entropy = document.getElementById("input_system_entropy");

const slider_system_arms = document.getElementById("slider_system_arms");
const input_system_arms = document.getElementById("input_system_arms");

const slider_system_twist = document.getElementById("slider_system_twist");
const input_system_twist = document.getElementById("input_system_twist");

const button_generate = document.getElementById("button_generate");


// Star system data
// World coordinates are centered at (0, 0) and extend from -100 <= x, y <= 100
var systems = [];
var system_num = 42;
var system_entropy = 5;
var system_arms = 4;
var system_twist = 30;

// Start everything from here
function main() {
  window.addEventListener('resize', function() {onResize();});
  window.addEventListener('orientationchange', function() {onResize();});

  slider_system_num.oninput = onNumChange;
  input_system_num.oninput = onNumChange;

  slider_system_entropy.oninput = onEntropyChange;
  input_system_entropy.oninput = onEntropyChange;

  slider_system_arms.oninput = onArmsChange;
  input_system_arms.oninput = onArmsChange;

  slider_system_twist.oninput = onTwistChange;
  input_system_twist.oninput = onTwistChange;

  button_generate.onclick = function() {
    systems = generateSpiral(system_num, system_entropy, system_arms, system_twist);
    draw();
  }
  c.addEventListener('mousedown', function(e) {
    const rect = canvas.getBoundingClientRect();
    const x = e.clientX - rect.left;
    const y = e.clientY - rect.top;
    const coord = screenToWorldCoords([x, y]);
    console.log("x: " + x + " y: " + y, coord, intersects(coord[0], coord[1], 1));
  });

  systems = generateSpiral()
  // Initialize the canvas dimensions
  onResize();
}

function worldToScreenCoords(coord) {
  return [(coord[0] + 100) * 0.005 * c.width, (coord[1]+100) * 0.005 * c.height];
}

function screenToWorldCoords(coord) {
  return [(coord[0] / c.width * 200) - 100, (coord[1] / c.height * 200) - 100];
}

function draw() {
  // Clear the screen
  ctx.clearRect(0, 0, c.width, c.height);

  for (system of systems) {
    coord = worldToScreenCoords(system.coord);

    ctx.beginPath();
    ctx.ellipse(coord[0], coord[1], 5, 5, 0, 0, 2 * Math.PI);
    ctx.stroke();
  }
}

function onResize() {
  const h = c.offsetHeight;
  const w = c.offsetWidth;

  c.height = h;
  c.width = w;

  draw();
}

function onNumChange() {
  system_num = this.value;
  slider_system_num.value = system_num;
  input_system_num.value = system_num;

  systems = generateSpiral();
  draw();
}

function onEntropyChange() {
  system_entropy = this.value;
  slider_system_entropy.value = system_entropy;
  input_system_entropy.value = system_entropy;

  systems = generateSpiral();
  draw();
}

function onArmsChange() {
  system_arms = this.value;
  slider_system_arms.value = system_arms;
  input_system_arms.value = system_arms;

  systems = generateSpiral();
  draw();
}

function onTwistChange() {
  system_twist = this.value;
  slider_system_twist.value = system_twist;
  input_system_twist.value = system_twist;

  systems = generateSpiral();
  draw();
}

// Generation
function generateSpiral() {
  const num = system_num;
  const entropy = system_entropy;
  const arms = system_arms;
  const twist = system_twist;

  const PI_2 = Math.PI * 2;
  const thetaEntropy = entropy * 0.75 / Math.PI;
  var systems = [];
  const thetaStep = PI_2 / num * twist / 30;
  const thetaStepArm = PI_2 / arms;

  // If we try to generate a non intersecting system and fail too many times,
  // Just give up and let the system intersect. The user can fix it manually if
  // they need to.
  const max_tries = 10;

  for (var i = 0; i < num; i++) {
    const arm = i % arms;
    var r; var theta; var x; var y;
    var tries = 0;

    do {
      tries += 1;
      // Push the r values out slightly using Chebyshev points
      r = 100 * Math.cos(i * Math.PI / num  * 0.5) + (Math.random() - 0.5) * entropy * tries;
      theta = i * thetaStep + arm * thetaStepArm + (Math.random() - 0.5) * thetaEntropy;
      x = r * Math.cos(theta);
      y = r * Math.sin(theta);
    } while (intersects(x, y, 2) && entropy > 1 && tries < max_tries);

    systems.push({coord: [x, y]});
  }

  return systems;
}

// Check whether these coordinates are within 2 * radius of any existing system.
// Time complexity: O(system_num)
function intersects(x, y, radius) {
  const diameter = 2 * radius;
  for (system of systems) {
    var coord = system.coord;
    var dist = Math.sqrt(Math.pow(coord[0]-x, 2) + Math.pow(coord[1]-y, 2));

    if (dist < diameter) {
      return true;
    }
  }
  return false;
}

// Setup
window.onload = main;
